#include <Mesh.hpp>
#include "Mesh.hpp"
#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <iostream>
#include <vector>

glm::vec3 countVertex( double a, double b, double u, double v ) {
    double c = sqrt(a*a - b*b);
    double d = c + 0.1;
    double x = (d*(c-a*cos(u)*cos(v)) + b*b*cos(u))/(a-c*cos(u)*cos(v));
    double y = b*sin(u)*(a-d*cos(v))/(a-c*cos(u)*cos(v));
    double z = b*sin(v)*(c*cos(u)-d)/(a-c*cos(u)*cos(v));
    return glm::vec3({x, y, z});
}

glm::vec3 countNormal( double a, double b, double u, double v ) {

    double c = sqrt(a*a - b*b);
    double d = c + 0.1;
    double denominator = a-c*cos(u)*cos(v);
    double denominator_dev_u = c*sin(u)*cos(v);
    double denominator_dev_v = c*cos(u)*sin(v);

    double x_dev_u = ( (d*a*sin(u)*cos(v)-b*b*sin(u))*denominator + denominator_dev_u * (d*(c-a*cos(u)*cos(v)) + b*b*cos(u))) / denominator/denominator;
    double x_dev_v = ( (d*a*cos(u)*sin(v))*denominator + denominator_dev_v * (d*(c-a*cos(u)*cos(v)) + b*b*cos(u))) / denominator/denominator;
    double y_dev_u = ( (b*cos(u)*(a-d*cos(v)))*denominator + denominator_dev_u * b*sin(u)*(a-d*cos(v))) / denominator/denominator;
    double y_dev_v = ( (b*sin(u)*d*sin(v))*denominator + denominator_dev_v * b*sin(u)*(a-d*cos(v)) ) / denominator/denominator;
    double z_dev_u = ( (-b*sin(v)*c*sin(u))*denominator + denominator_dev_u * (b*sin(v)*(c*cos(u)-d))) / denominator/denominator;
    double z_dev_v = ( (b*cos(v)*(c*cos(u)-d))*denominator + denominator_dev_v * (b*sin(v)*(c*cos(u)-d))) / denominator/denominator;

    glm::vec3 dev_u = glm::vec3( x_dev_u, y_dev_u, z_dev_u );
    glm::vec3 dev_v = glm::vec3( x_dev_v, y_dev_v, z_dev_v );
    return glm::normalize( glm::cross( dev_u, dev_v ) );
}

MeshPtr makeCyclide(int N, double a, double b, double u_range_from, double v_range_from, double u_range_to, double v_range_to, double delta_u, double delta_v)
{
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;

    for( double u = u_range_from; u < u_range_to; u += delta_u ) {
        for( double v = v_range_from; v < v_range_to; v += delta_v ) {
            double next_v_triangle = v + delta_v > v_range_to ? v_range_to : v + delta_v;
            double next_u_triangle = u + delta_u > u_range_to ? u_range_to : u + delta_u;
            glm::vec3 vertex1 = countVertex(a,b, u, v);
            glm::vec3 vertex2 = countVertex(a,b, u, next_v_triangle);
            glm::vec3 vertex3 = countVertex(a,b, next_u_triangle, v);
            glm::vec3 vertex4 = countVertex(a,b, next_u_triangle, next_v_triangle);

            glm::vec3 normal1 = countNormal(a,b, u, v);
            glm::vec3 normal2 = countNormal(a,b, u, next_v_triangle);
            glm::vec3 normal3 = countNormal(a,b, next_u_triangle, v);
            glm::vec3 normal4 = countNormal(a,b, next_u_triangle, next_v_triangle);

            //2 triangles (vertices): {1,2,3} and {2,3,4}
            vertices.push_back(vertex1);
            vertices.push_back(vertex2);
            vertices.push_back(vertex3);
            vertices.push_back(vertex2);
            vertices.push_back(vertex3);
            vertices.push_back(vertex4);

            //2 triangles (normals): {1,2,3} and {2,3,4}
            normals.push_back(normal1);
            normals.push_back(normal2);
            normals.push_back(normal3);
            normals.push_back(normal2);
            normals.push_back(normal3);
            normals.push_back(normal4);
        }
    }

    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());
    
    MeshPtr surface = std::make_shared<Mesh>();
    surface->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    surface->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    surface->setPrimitiveType(GL_TRIANGLES);
    surface->setVertexCount(vertices.size());

    std::cout << "Cyclide is created with " << vertices.size() << " vertices\n";

    return surface;
}
