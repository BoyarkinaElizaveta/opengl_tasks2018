#include "Application.hpp"
#include "Mesh.hpp"
#include "ShaderProgram.hpp"

#include <vector>
#include <iostream>

class Cyclide : public Application
{
public:

    float delta_u;
    float delta_v;
    float a;
    float b;
    float u_range_from;
    float u_range_to;
    float v_range_from;
    float v_range_to;

	MeshPtr _cyclide;

	ShaderProgramPtr _shader;

    Cyclide()
            : delta_u(0.1), delta_v(0.1), a(4.0),b(3.8),
            u_range_from(0), u_range_to(2.0 * glm::pi<float>()),
            v_range_from(0), v_range_to(2.0 * glm::pi<float>()) {}


	void makeScene() override
	{
		Application::makeScene();

		_cameraMover = std::make_shared<OrbitCameraMover>();

		//Создаем меш с лентой Мёбиуса
		_cyclide = makeCyclide(100, a, b, u_range_from, v_range_from, u_range_to, v_range_to, delta_u, delta_v);
		_cyclide->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));

		//Создаем шейдерную программу        
		_shader = std::make_shared<ShaderProgram>("497BoyarkinaData/shaderNormal.vert", "497BoyarkinaData/shader.frag");
	}
	void draw() override
	{
		Application::draw();

		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);

		glViewport(0, 0, width, height);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//Устанавливаем шейдер.
		_shader->use();

		//Устанавливаем общие юниформ-переменные
		_shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		_shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

		//Рисуем первый меш
		_shader->setMat4Uniform("modelMatrix", _cyclide->modelMatrix());
		_cyclide->draw();

	}

	void handleKey(int key, int scancode, int action, int mods) override
	{
	    Application::handleKey(key, scancode, action, mods);

	    const double multiplier = 0.5;

	    if (action == GLFW_PRESS) {
		    if (key == GLFW_KEY_MINUS) {
		        delta_u *= ( 1.0 / multiplier );
		        delta_v *= ( 1.0 / multiplier );
		        makeScene();
		    }
		    if (key == GLFW_KEY_EQUAL) {
		        delta_u *= multiplier;
		        delta_v *= multiplier;
		        makeScene();
		    }
	    }
	}


};

int main()
{
	Cyclide app;
	app.start();
	return 0;
}
