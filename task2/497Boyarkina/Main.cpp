#include "Application.hpp"
#include "Mesh.hpp"
#include "ShaderProgram.hpp"
#include "Texture.hpp"

#include <vector>
#include <iostream>
#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

const double EPSILON = 0.000001;

class Cyclide : public Application
{
public:

    float delta_u;
    float delta_v;
    float a;
    float b;
    float u_range_from;
    float u_range_to;
    float v_range_from;
    float v_range_to;
    
    bool drawing;

	MeshPtr _cyclide;

	ShaderProgramPtr _shader;

	TextureImage image;
    TexturePtr texture;
    GLuint sampler;

    Cyclide()
            : delta_u(0.1), delta_v(0.1), a(2.0), b(1.9),
            u_range_from(0), u_range_to(2.0 * glm::pi<float>()),
            v_range_from(0), v_range_to(2.0 * glm::pi<float>()),
            drawing( false ) {}

    void StartDrawing() { drawing = true; }
    void StopDrawing() { drawing = false; }
    bool isDrawing() { return drawing; }

	void makeScene() override
	{
		Application::makeScene();

		_cameraMover = std::make_shared<OrbitCameraMover>();

		//Создаем меш
		_cyclide = makeCyclide(100, a, b, u_range_from, v_range_from, u_range_to, v_range_to, delta_u, delta_v);
		_cyclide->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));

		//Создаем шейдерную программу        
		_shader = std::make_shared<ShaderProgram>("497BoyarkinaData/texture.vert", "497BoyarkinaData/texture.frag");

		image = loadImage( "./497BoyarkinaData/flowers.jpg" );
	    texture = loadTexture( image );

	    glGenSamplers( 1, &sampler );
	    glSamplerParameteri( sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
	    glSamplerParameteri( sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
	    glSamplerParameteri( sampler, GL_TEXTURE_WRAP_S, GL_REPEAT );
	    glSamplerParameteri( sampler, GL_TEXTURE_WRAP_T, GL_REPEAT );
	}
	void updateTexture()
	{
		texture = loadTexture( image );
	}
	void draw() override
	{
		Application::draw();

        if( drawing ) {
            updateTexture();
        }

		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);

		glViewport(0, 0, width, height);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//Устанавливаем шейдер.
		_shader->use();

		//Устанавливаем общие юниформ-переменные
		_shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		_shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

		glActiveTexture( GL_TEXTURE0 );
		glBindSampler( 0, sampler );
		texture->bind();
		_shader->setIntUniform("textureSampler", 0);

		_shader->setMat3Uniform("normalToCameraMatrix",
		                       glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _cyclide->modelMatrix()))));

		//Рисуем первый меш
		_shader->setMat4Uniform("modelMatrix", _cyclide->modelMatrix());
		_cyclide->draw();

		glBindSampler(0, 0);
		glUseProgram(0);

	}

	void handleKey(int key, int scancode, int action, int mods) override
	{
	    Application::handleKey(key, scancode, action, mods);

	    const double multiplier = 0.5;

	    if (action == GLFW_PRESS) {
		    if (key == GLFW_KEY_MINUS) {
		        delta_u *= ( 1.0 / multiplier );
		        delta_v *= ( 1.0 / multiplier );
		        makeScene();
		    }
		    if (key == GLFW_KEY_EQUAL) {
		        delta_u *= multiplier;
		        delta_v *= multiplier;
		        makeScene();
		    }
	    }
	}
	
	void handleRightMouseButton( int action )
	{
        if( action == GLFW_PRESS ) {
            StartDrawing();
        } else if( action == GLFW_RELEASE ) {
            StopDrawing();
        }
    }

    void handleMouseMove(double xpos, double ypos ) override
    {
        if (ImGui::IsMouseHoveringAnyWindow())
        {
            return;
        }

        if( drawing ) {
            DrawOnCyclide();
        } else {
            _cameraMover->handleMouseMove(_window, xpos, ypos);
        }
    }
    
    void DrawOnCyclide()
    {
        double x, y, z;
        glfwGetCursorPos(_window, &x, &y);

        GLint viewport[4];
        glGetIntegerv(GL_VIEWPORT, viewport);

        glm::mat4 matrix = glm::inverse( _camera.projMatrix * _camera.viewMatrix * _cyclide->modelMatrix() );

        glm::vec4 vector1;
        glm::vec4 vector2;

        vector1[0] = 2.0 * ( x - viewport[0] ) / viewport[2] - 1.0;
        vector1[1] = 1.0 - 2.0 * ( y - viewport[1] ) / viewport[3];
        vector1[2] = 0.0;
        vector1[3] = 1.0;

        vector2 = vector1;
        vector2[2] = 1.0;

        glm::vec4 position1 = matrix * vector1;
        glm::vec4 position2 = matrix * vector2;

        if( findTriangleToDraw( normalize( position1 ), normalize( position2 ) ) ) {
            draw();
        }
    }
    
    glm::vec3 normalize( glm::vec4 vec )
    {
        glm::vec3 result;
        result.x = vec.x / vec.w;
        result.y = vec.y / vec.w;
        result.z = vec.z / vec.w;
        return result;
    }
    
    bool findTriangleToDraw( glm::vec3 startRay, glm::vec3 endRay )
    {
        bool isAnyCandidateFound = false;
        double bestCoeff = std::numeric_limits<double>::max();
        glm::vec2 bestCandidate;

        for( double u = u_range_from; u < u_range_to; u += delta_u ) {
            for( double v = v_range_from; v < v_range_to; v += delta_v ) {
                double next_v = v + delta_v > v_range_to ? v_range_to : v + delta_v;
                double next_u = u + delta_u > u_range_to ? u_range_to : u + delta_u;
                glm::vec3 vertex1 = countVertex(a,b, u, v);
                glm::vec3 vertex2 = countVertex(a,b, u, next_v);
                glm::vec3 vertex3 = countVertex(a,b, next_u, v);
                glm::vec3 vertex4 = countVertex(a,b, next_u, next_v);

                // normal to plane of first triangle
                glm::vec3 normal1 = glm::cross( vertex2 - vertex1, vertex3 - vertex1 );
                glm::vec3 normal2 = glm::cross( vertex2 - vertex4, vertex3 - vertex4 );

                double denominator1 = glm::dot( endRay - startRay, normal1 );
                double denominator2 = glm::dot( endRay - startRay, normal2 );

                //check if ray and triangle1 are parallel
                if( glm::abs( denominator1 ) >= EPSILON )
                {
                    double scaleCoeff = glm::dot( normal1, vertex1 - startRay ) / denominator1;
                    glm::vec3 intersectionPoint = startRay + scaleVector( scaleCoeff, endRay - startRay );

                    double s, t;
                    if( checkIfPointInTriangle( intersectionPoint, vertex1, vertex3, vertex2, s, t ) ) {

                        if( !isAnyCandidateFound || bestCoeff > scaleCoeff ) {
                            isAnyCandidateFound = true;
                            bestCoeff = scaleCoeff;

                            glm::vec2 texCoord = { ( u - u_range_from ) / ( u_range_to - u_range_from ),
                                                   ( v - v_range_from ) / ( v_range_to - v_range_from ) };
                            bestCandidate = addShift( texCoord, s, t );
                        }
                    }
                }

                //check if ray and triangle2 are parallel
                if( glm::abs( denominator2 ) >= EPSILON )
                {
                    double scaleCoeff = glm::dot( normal2, vertex4 - startRay ) / denominator2;
                    glm::vec3 intersectionPoint =  startRay + scaleVector( scaleCoeff, endRay - startRay );

                    double s, t;
                    if( checkIfPointInTriangle( intersectionPoint, vertex4, vertex2, vertex3, s, t ) ) {

                        if( !isAnyCandidateFound || bestCoeff > scaleCoeff ) {
                            isAnyCandidateFound = true;
                            bestCoeff = scaleCoeff;

                            glm::vec2 texCoord = { ( next_u - u_range_from ) / ( u_range_to - u_range_from ),
                                                   ( next_v - v_range_from ) / ( v_range_to - v_range_from ) };
                            bestCandidate = addShift( texCoord, -s, -t );
                        }
                    }
                }
            }
        }
        if( isAnyCandidateFound ) {
            modifyImage( bestCandidate[0], bestCandidate[1] );
        }
        return isAnyCandidateFound;
    }
    
    glm::vec3 scaleVector( double coeff, glm::vec3 vec )
    {
        glm::vec3 result = vec;
        result.x *= coeff;
        result.y *= coeff;
        result.z *= coeff;
        return result;
    }
    
    bool checkIfPointInTriangle( glm::vec3 intersectionPoint,
                                                 glm::vec3 v0,  glm::vec3 v1, glm::vec3 v2,
                                                 double& s, double& t )
    {
        glm::vec3 u = v1 - v0;
        glm::vec3 v = v2 - v0;
        glm::vec3 w = intersectionPoint - v0;

        double uu = glm::dot( u, u );
        double vv = glm::dot( v, v );
        double uv = glm::dot( u, v );
        double wv = glm::dot( w, v );
        double wu = glm::dot( w, u );

        s = ( uv * wv - vv * wu ) / ( uv * uv - uu * vv );
        t = ( uv * wu - uu * wv ) / ( uv * uv - uu * vv );

        return s >= 0.0 && t >= 0.0 && s + t <= 1.0;
    }
    
    void modifyImage( double u, double v, glm::vec3 color = glm::vec3({ 0, 0, 0 }) )
    {
        int x = image.width * u;
        int y = image.height * v;
        int startIndex = ( y * image.width + x ) * image.channels;
        int endIndex = startIndex + image.channels;
        for( int index = startIndex; index < endIndex; index++ ) {
            image.data[index] = color[index % image.channels];
        }
    }
    
    glm::vec2 addShift( glm::vec2 texCoord, double s, double t )
    {
        texCoord[0] += s * delta_u / ( u_range_to - u_range_from );
        texCoord[1] += t * delta_v / ( v_range_to - v_range_from );

        texCoord[0] = std::min<float>( texCoord[0], 1.0 );
        texCoord[1] = std::min<float>( texCoord[1], 1.0 );
        texCoord[0] = std::max<float>( texCoord[0], 0.0 );
        texCoord[1] = std::max<float>( texCoord[1], 0.0 );
        return texCoord;
    }

    glm::vec3 countVertex( double a, double b, double u, double v ) {
        double c = sqrt(a*a - b*b);
        double d = c + 0.1;
        double x = (d*(c-a*cos(u)*cos(v)) + b*b*cos(u))/(a-c*cos(u)*cos(v));
        double y = b*sin(u)*(a-d*cos(v))/(a-c*cos(u)*cos(v));
        double z = b*sin(v)*(c*cos(u)-d)/(a-c*cos(u)*cos(v));
        return glm::vec3({x, y, z});
    }

    glm::vec3 countNormal( double a, double b, double u, double v ) {

        double c = sqrt(a*a - b*b);
        double d = c + 0.1;
        double denominator = a-c*cos(u)*cos(v);
        double denominator_dev_u = c*sin(u)*cos(v);
        double denominator_dev_v = c*cos(u)*sin(v);

        double x_dev_u = ( (d*a*sin(u)*cos(v)-b*b*sin(u))*denominator + denominator_dev_u * (d*(c-a*cos(u)*cos(v)) + b*b*cos(u))) / denominator/denominator;
        double x_dev_v = ( (d*a*cos(u)*sin(v))*denominator + denominator_dev_v * (d*(c-a*cos(u)*cos(v)) + b*b*cos(u))) / denominator/denominator;
        double y_dev_u = ( (b*cos(u)*(a-d*cos(v)))*denominator + denominator_dev_u * b*sin(u)*(a-d*cos(v))) / denominator/denominator;
        double y_dev_v = ( (b*sin(u)*d*sin(v))*denominator + denominator_dev_v * b*sin(u)*(a-d*cos(v)) ) / denominator/denominator;
        double z_dev_u = ( (-b*sin(v)*c*sin(u))*denominator + denominator_dev_u * (b*sin(v)*(c*cos(u)-d))) / denominator/denominator;
        double z_dev_v = ( (b*cos(v)*(c*cos(u)-d))*denominator + denominator_dev_v * (b*sin(v)*(c*cos(u)-d))) / denominator/denominator;

        glm::vec3 dev_u = glm::vec3( x_dev_u, y_dev_u, z_dev_u );
        glm::vec3 dev_v = glm::vec3( x_dev_v, y_dev_v, z_dev_v );
        return glm::normalize( glm::cross( dev_u, dev_v ) );
    }

    MeshPtr makeCyclide(int N, double a, double b, double u_range_from, double v_range_from, double u_range_to, double v_range_to, double delta_u, double delta_v)
    {
        std::vector<glm::vec3> vertices;
        std::vector<glm::vec3> normals;
        std::vector<glm::vec2> textureCoords;

        for( double u = u_range_from; u < u_range_to; u += delta_u ) {
            for( double v = v_range_from; v < v_range_to; v += delta_v ) {
                double next_v_triangle = v + delta_v > v_range_to ? v_range_to : v + delta_v;
                double next_u_triangle = u + delta_u > u_range_to ? u_range_to : u + delta_u;
                glm::vec3 vertex1 = countVertex(a,b, u, v);
                glm::vec3 vertex2 = countVertex(a,b, u, next_v_triangle);
                glm::vec3 vertex3 = countVertex(a,b, next_u_triangle, v);
                glm::vec3 vertex4 = countVertex(a,b, next_u_triangle, next_v_triangle);

                glm::vec3 normal1 = countNormal(a,b, u, v);
                glm::vec3 normal2 = countNormal(a,b, u, next_v_triangle);
                glm::vec3 normal3 = countNormal(a,b, next_u_triangle, v);
                glm::vec3 normal4 = countNormal(a,b, next_u_triangle, next_v_triangle);

                glm::vec2 texCoord1 = { ( u - u_range_from ) / ( u_range_to - u_range_from ),
                                        ( v - v_range_from ) / ( v_range_to - v_range_from ) };
                glm::vec2 texCoord2 = { ( u - u_range_from ) / ( u_range_to - u_range_from ),
                                        ( next_v_triangle - v_range_from ) / ( v_range_to - v_range_from ) };
                glm::vec2 texCoord3 = { ( next_u_triangle - u_range_from ) / ( u_range_to - u_range_from ),
                                        ( v - v_range_from ) / ( v_range_to - v_range_from ) };
                glm::vec2 texCoord4 = { ( next_u_triangle - u_range_from ) / ( u_range_to - u_range_from ),
                                        ( next_v_triangle - v_range_from ) / ( v_range_to - v_range_from ) };


                //2 triangles (vertices): {1,2,3} and {2,3,4}
                vertices.push_back(vertex1);
                vertices.push_back(vertex2);
                vertices.push_back(vertex3);
                vertices.push_back(vertex2);
                vertices.push_back(vertex3);
                vertices.push_back(vertex4);

                //2 triangles (normals): {1,2,3} and {2,3,4}
                normals.push_back(normal1);
                normals.push_back(normal2);
                normals.push_back(normal3);
                normals.push_back(normal2);
                normals.push_back(normal3);
                normals.push_back(normal4);
                
                textureCoords.push_back(texCoord1);
                textureCoords.push_back(texCoord2);
                textureCoords.push_back(texCoord3);
                textureCoords.push_back(texCoord2);
                textureCoords.push_back(texCoord3);
                textureCoords.push_back(texCoord4);
            }
        }

        //----------------------------------------

        DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

        DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf1->setData(normals.size() * sizeof(float) * 3, normals.data());
        
        DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf2->setData(textureCoords.size() * sizeof(float) * 2, textureCoords.data());
        
        MeshPtr surface = std::make_shared<Mesh>();
        surface->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
        surface->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
        surface->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
        
        surface->setPrimitiveType(GL_TRIANGLES);
        surface->setVertexCount(vertices.size());

        std::cout << "Cyclide is created with " << vertices.size() << " vertices\n";

        return surface;
    }

};

int main()
{
	Cyclide app;
	app.start();
	return 0;
}
