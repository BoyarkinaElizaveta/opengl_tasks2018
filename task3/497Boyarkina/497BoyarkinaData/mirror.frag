/*
Получает на вход интеполированный цвет фрагмента и копирует его на выход.
*/

#version 330

uniform sampler2D textureSampler;

in vec2 textureCoord;

out vec4 fragColor;

void main()
{
    fragColor = vec4( texture(textureSampler, textureCoord).rgb, 1.0);
}
