/*
Простейший вершинный шейдер для первого семинара. Подробности - в семинаре №2
*/

#version 330

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

uniform mat3 normalToCameraMatrix;

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;
layout(location = 2) in vec2 vertexTextureCoord;

out vec3 normalCamSpace; //нормаль в системе координат камеры
out vec4 posCamSpace; //координаты вершины в системе координат камеры
out vec2 textureCoord; //текстурные координаты

void main()
{
    textureCoord = vertexTextureCoord;

    posCamSpace = viewMatrix * modelMatrix * vec4(vertexPosition, 1.0); //преобразование координат вершины в систему координат камеры
    normalCamSpace = normalize(normalToCameraMatrix * vertexNormal); //преобразование нормали в систему координат камеры

    if( dot( normalCamSpace, posCamSpace.xyz ) < 0) {
        normalCamSpace *= -1.0;
    }

    gl_Position = projectionMatrix * posCamSpace;
}
